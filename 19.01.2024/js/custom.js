$(".box-switch").on("click", () => {
   $("body").toggleClass("dark");
});


// header class add
 $(window).on("scroll", function() {
     if($(window).scrollTop() > 50) {
         $("header").addClass("newheader");
     } else {
        $("header").removeClass("newheader");
     }
 });


 // click to top
 $(document).ready(function(){
   "use strict";
  var offSetTop = 100;
  var $scrollToTopButton = $('.scrollToTop');
   //Check to see if the window is top if not then display button
   $(window).scroll(function(){
      if ($(this).scrollTop() > offSetTop) {
         $scrollToTopButton.fadeIn();
      } else {
         $scrollToTopButton.fadeOut();
      }
   });
   
   //Click event to scroll to top
   $scrollToTopButton.click(function(){
      $('html, body').animate({scrollTop : 0},800);
      return false;
   });
   
});